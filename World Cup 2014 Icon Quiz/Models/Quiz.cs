﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Cup_2014_Icon_Quiz.Models
{
    public class Quiz
    {
        public int id { get; set; }
        public string image { get; set; }
        public string answer { get; set; }
        public bool solved { get; set; }
    }
}
