﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using World_Cup_2014_Icon_Quiz.Models;
using Microsoft.Phone.Tasks;

namespace World_Cup_2014_Icon_Quiz.Pages
{
    public partial class HighscorePage : PhoneApplicationPage
    {
        private int solved;
        private int total;
        public HighscorePage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            List<Quiz> list = new List<Quiz>();
            for (int i = 0; i < 3; i++)
            {
                list.AddRange((List<Quiz>)settings[(i + 1).ToString()]);
            }
            total = list.Count;
            list = list.FindAll(item => item.solved == true);
            solved = list.Count;
            tbSolved.Text = solved.ToString();
            tbUnsolved.Text = (total - solved).ToString();
        }

        private void onClickShare(object sender, EventArgs e)
        {
            ShareStatusTask sst = new ShareStatusTask();
            sst.Status = "I have completed " + solved + " in World Cup 2014 Icon Quiz!";
            sst.Show();
        }

        private void onClickResetHighScore(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to reset your highscore and your game?", "Reset", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                settings.Clear();
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
            else
                return;
        }
    }
}