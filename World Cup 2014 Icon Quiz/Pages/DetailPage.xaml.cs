﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using World_Cup_2014_Icon_Quiz.Models;
using System.IO.IsolatedStorage;
using System.Windows.Media.Imaging;

namespace World_Cup_2014_Icon_Quiz.Pages
{
    public partial class DetailPage : PhoneApplicationPage
    {
        private int level;
        private int id;
        private string correctAnswer;
        private List<Quiz> list;
        Quiz quiz;
        public DetailPage()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.ContainsKey("level") && NavigationContext.QueryString.ContainsKey("id"))
            {
                level = Int32.Parse(this.NavigationContext.QueryString["level"]);
                id = Int32.Parse(this.NavigationContext.QueryString["id"]);
            }
            loadImage();
        }

        private void loadImage()
        {
            tbTitle.Text = "Level " + level.ToString();
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            list = (List<Quiz>)settings[level.ToString()];
            quiz = list[id];
            iQuestion.Source = new BitmapImage(new Uri(quiz.image, UriKind.Relative));
            correctAnswer = quiz.answer;
            if (quiz.solved == true)
            {
                tbAnswer.IsEnabled = false;
                tbAnswer.Text = quiz.answer;
            }
        }

        private void OnClickCheck(object sender, RoutedEventArgs e)
        {
            string useranswer = tbAnswer.Text;
            useranswer = useranswer.ToLower();
            string correct = quiz.answer.ToLower();
            if (useranswer == correct)
            {
                MessageBox.Show("Correct!");
                quiz.solved = true;
                list[id] = quiz;
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                settings[level.ToString()] = list;
                tbAnswer.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("False!");
            }
        }
    }
}