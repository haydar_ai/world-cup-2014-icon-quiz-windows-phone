﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace World_Cup_2014_Icon_Quiz.Pages
{
    public partial class HelpPage : PhoneApplicationPage
    {
        public HelpPage()
        {
            InitializeComponent();
        }

        private void OnClickNext(object sender, RoutedEventArgs e)
        {
            pTutorial.IsLocked = false;
            pTutorial.SelectedIndex++;
            pTutorial.IsLocked = true;
        }

        private void OnClickFinish(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}