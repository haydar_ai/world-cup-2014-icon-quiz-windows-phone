﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using World_Cup_2014_Icon_Quiz.Models;

namespace World_Cup_2014_Icon_Quiz.Pages
{
    public partial class LevelPage : PhoneApplicationPage
    {
        private int level;
        public LevelPage()
        {
            InitializeComponent();
        }

        private void loadLevel(int level)
        {
            tbTitle.Text = "Level " + level.ToString();
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            List<Quiz> levels = (List<Quiz>)settings[level.ToString()];
            lbLevel.ItemsSource = levels;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.ContainsKey("level"))
            {
                level = Int32.Parse(this.NavigationContext.QueryString["level"]);
            }
            loadLevel(level);
        }

        private void OnLevelItemClick(object sender, SelectionChangedEventArgs e)
        {
            if (lbLevel.SelectedItem == null)
                return;
            NavigationService.Navigate(new Uri("/Pages/DetailPage.xaml?level=" + level.ToString() + "&id=" + lbLevel.SelectedIndex, UriKind.Relative));
            lbLevel.SelectedItem = null;
        }
    }
}