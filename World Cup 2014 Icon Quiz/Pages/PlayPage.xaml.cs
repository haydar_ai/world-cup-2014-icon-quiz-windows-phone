﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace World_Cup_2014_Icon_Quiz.Pages
{
    public partial class PlayPage : PhoneApplicationPage
    {
        public PlayPage()
        {
            InitializeComponent();
        }

        private void OnClickLevelOne(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/LevelPage.xaml?level=1", UriKind.Relative));
        }

        private void OnClickLevelTwo(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/LevelPage.xaml?level=2", UriKind.Relative));
        }

        private void OnClickLevelThree(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/LevelPage.xaml?level=3", UriKind.Relative));
        }
    }
}