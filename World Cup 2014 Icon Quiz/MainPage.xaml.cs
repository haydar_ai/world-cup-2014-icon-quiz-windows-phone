﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using World_Cup_2014_Icon_Quiz.Resources;
using System.IO.IsolatedStorage;
using World_Cup_2014_Icon_Quiz.Models;

namespace World_Cup_2014_Icon_Quiz
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        private string[] answerLevelOne;
        private string[] answerLevelTwo;
        private string[] answerLevelThree;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            loadLevel();
        }

        private void loadLevel()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            if (!settings.Contains("1"))
            {
                loadLevelOne();
            }
            if (!settings.Contains("2"))
            {
                loadLevelTwo();
            }
            if (!settings.Contains("3"))
            {
                loadLevelThree();
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if (e.NavigationMode == NavigationMode.Back)
            {
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                List<Quiz> list = new List<Quiz>();
                for (int i = 0; i < 3; i++)
                {
                    list.AddRange((List<Quiz>)settings[(i + 1).ToString()]);
                }
                int total = list.Count;
                list = list.FindAll(item => item.solved == true);
                int solved = list.Count;
                int unsolved = total - solved;

                ShellTile tile = ShellTile.ActiveTiles.FirstOrDefault();
                FlipTileData fliptile = new FlipTileData();
                fliptile.Title = "WC 2014 Icon Quiz";
                fliptile.Count = unsolved;
                fliptile.BackTitle = "World Cup 2014 Icon Quiz";

                fliptile.BackContent = "You have solved " + solved + " quiz";
                fliptile.WideBackContent = "You have solved " + solved + " quiz";

                fliptile.SmallBackgroundImage = new Uri("159.png", UriKind.Relative);
                fliptile.BackgroundImage = new Uri("336.png", UriKind.Relative);
                fliptile.WideBackgroundImage = new Uri("691.png", UriKind.Relative);

                tile.Update(fliptile);
            }
        }

        private void loadLevelOne()
        {
            List<Quiz> levels = new List<Quiz>();
            answerLevelOne = new string[] { "Neymar", "Etoo", "Xavi", "Alexis", "Falcao", "Suarez", "Ribery", "Dzeko", "Cristiano" };
            for (int i = 0; i < 9; i++)
            {
                levels.Add(new Quiz { id = (i + 1), image = "/Assets/Levels/1/" + (i + 1) + ".png", solved = false, answer = answerLevelOne[i] });
            }
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            settings.Add("1", levels);
        }

        public void loadLevelTwo()
        {
            List<Quiz> levels = new List<Quiz>();
            answerLevelTwo = new string[] { "Ecuador", "Uruguay", "Spain", "Brazil", "Chile", "Cameroon", "Belgium", "Croatia", "France" };
            for (int i = 0; i < 9; i++)
            {
                levels.Add(new Quiz { id = (i + 1), image = "/Assets/Levels/2/" + (i + 1) + ".jpg", solved = false, answer = answerLevelTwo[i] });
            }
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            settings.Add("2", levels);
        }

        public void loadLevelThree()
        {
            List<Quiz> levels = new List<Quiz>();
            answerLevelThree = new string[] { "Modric", "Chicarito", "Van Persie", "Cahill", "Drogba", "Balotelli", "Messi", "Ozil", "Hazard" };
            for (int i = 0; i < 9; i++)
            {
                levels.Add(new Quiz { id = (i + 1), image = "/Assets/Levels/3/" + (i + 1) + ".png", solved = false, answer = answerLevelThree[i] });
            }
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            settings.Add("3", levels);
        }

        private void OnClickPlay(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/PlayPage.xaml", UriKind.Relative));
        }

        private void OnClickHighScore(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/HighscorePage.xaml", UriKind.Relative));
        }

        private void OnClickHelp(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/HelpPage.xaml", UriKind.Relative));
        }

        private void onClickAbout(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/AboutPage.xaml", UriKind.Relative));
        }
    }
}